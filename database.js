const mongoose = require('mongoose');

const URI = 'mongodb://localhost/proyecto-casa';

mongoose.connect(URI)
    .then(db => console.log('Db connect'))
    .catch(err => console.log(err));

module.exports = mongoose;