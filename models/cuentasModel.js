const mongoose = require('mongoose');
const { Schema } = mongoose;

const cuentasSchema = new Schema({
    gasto: { type: Number, required: true },
    product_service: { type: String, required: true },
    tipo: { type:String, require: true },
    fecha: { type: Date, required: false },
    nombre_usurio: { type: String, required: true },
    descripcion: { type: String, require: false }
})

module.exports = mongoose.model('Cuentas', cuentasSchema);
