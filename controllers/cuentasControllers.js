const cuentasModel = require('../models/cuentasModel');
const cuentasCtrl = {};

//Las consultas a la BD son asyncronas y toman un tiempo
// debemos emplear metodos asyncronos de JS, ya sea un callBack
//una Promise o como he usado AsyncFunctions que se basa en promesas tambien.

cuentasCtrl.getCuentas = async (req, res) => {
    const cuentas = await cuentasModel.find();
    res.json(cuentas);
}

//req.body trae el cuerpo de la solicitud en JSON
cuentasCtrl.createCuentas = async (req, res) => {
    const cuenta = new cuentasModel(req.body);
    await cuenta.save();
    res.json({
        'status': 'Cuenta Salvada'
    });
}

//nota: los metodos empleados por cuentasModel (Schema de mongoouse que usa mongoDB) 
//son de mongoose.
cuentasCtrl.getOneCuenta = async (req, res) => {
    //req.params.id retorna el id que se pasas= en el nav, sin .id retorna el {}
    const cuenta = await cuentasModel.findById(req.params.id);
    res.json(cuenta);
}

//existen otras formas de hacer actualizaciones... ver mas adelante
cuentasCtrl.editCuenta = async (req, res) => {
    //otra forma de obterner id
    const { id } = req.params;
    const cuenta = {
        gasto: req.body.gasto,
        product_service: req.body.product_service,
        tipo: req.body.tipo,
        fecha: req.body.fecha,
        nombre_usurio: req.body.nombre_usurio,
        descripcion: req.body.descripcion
    };
    await cuentasModel.findByIdAndUpdate(id, {$set: cuenta}, {new: true });
    res.json({
        status: 'Cuenta Actualizada'
    });
}

cuentasCtrl.deleteCuenta = async (req, res) => {
    const { id } = req.params;
    await cuentasModel.findByIdAndRemove(id)
    res.json({
        status: 'Cuenta Eliminada'
    });
}

module.exports = cuentasCtrl;