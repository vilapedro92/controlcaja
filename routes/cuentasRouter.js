const express = require('express');
const router = express.Router();
const cuentasControllers = require('../controllers/cuentasControllers');

router.get('/', cuentasControllers.getCuentas);
router.post('/', cuentasControllers.createCuentas);
router.get('/:id', cuentasControllers.getOneCuenta);
router.put('/:id', cuentasControllers.editCuenta);
router.delete('/:id', cuentasControllers.deleteCuenta);

module.exports = router;